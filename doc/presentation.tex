\documentclass{beamer}
\usetheme{Boadilla}
\usepackage{amsmath,amsfonts,amssymb,stmaryrd}
\usepackage{algpseudocode,algorithm}
\usepackage{hyperref}

\newcommand{\toolname}{RESIS}
\newcommand{\rationals}{\mathbb{R}}
\newcommand{\rationalspp}{\mathbb{R}^+}
\newcommand{\lpop}[3]{\mathrm{LP}_{#1,#2}(#3)}
\newcommand{\maxop}{\vee}
\newcommand{\minop}{\wedge}
\newcommand{\X}{\mathbf{X}}
\newcommand{\E}{\mathbf{E}}
%\newcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\syseq}{\varepsilon}
\newcommand{\transp}{\intercal}
\newcommand{\lpt}[1]{{\llbracket#1\rrbracket}_\mathrm{LP}}
\newcommand{\map}[2]{{[#1]}_{#2}}


\begin{document}

\begin{frame}{Outline}
  \tableofcontents
\end{frame}

\section{Rational Equations}
\subsection{Definition}
\begin{frame}{Systems of Rational Equations}
  
  $\epsilon = \{x = e\}$ where $x$ ranges over a set of variables $X$ and $e$
  is of the form:
  \begin{align*}
    e ::= &\, x \,|\, k \,|\, e + e \,|\, c \cdot x \\
        | &\, e \lor e \,|\, e \land e \,|\, LP_{A,c}(\mathbf{e})
  \end{align*}
  
  Where $x \in X$, $k \in \mathbb{R} \cup \{\infty,-\infty\}$, $c \in
  \mathbb{R}_{\ge 0}$
  \pause
  
  $e_1 \lor e_2$ is the \emph{maximum} between $e_1$ and $e_2$,\\
  $e_1 \land e_2$ is the \emph{minimum} between $e_1$ and $e_2$.\pause
  
  $$\lpop{A}{c}{e_1,\dots,e_n} = \max\{ c^\transp \mathbf{y} \mid A \mathbf{y}
  \leq \mathbf{e}\}$$\pause
  
  All the operations in these systems \emph{preserve monotonicity}.
\end{frame}

\subsection{Strategy}
\begin{frame}{Strategy}
  We define a strategy $\sigma$ as a choice over $\lor$-expressions:
  $$\sigma = \left\{e_1 \maxop e_2 \to e_i \,|\, i \in \{1,2\},\, e_1 \maxop e_2
  \in \syseq \right\}$$  
  and derive a system of rational equations without $\lor$-expressions with the
  following rules:
{\small
  \begin{align*}
    \map \syseq \sigma           &= \{x = \map e \sigma \,|\, x = e \in \syseq\}&
    \map {e_1 \minop e_2} \sigma &= \map {e_1} \sigma \minop \map {e_2} \sigma\\
    \map {e_1 \maxop e_2} \sigma &=
      \begin{cases}
        \map {e_1} \sigma & \text{iff } \sigma(e_1 \maxop e_2) = e_1 \\
        \map {e_2} \sigma & \text{iff } \sigma(e_1 \maxop e_2) = e_2
      \end{cases}&
    \map {\lpop{A}{c}{\vec{e}}} \sigma &= \lpop{A}{c}{\map {\vec{e}} \sigma} \\
    \map {e_1 + e_2} \sigma      &= \map {e_1} \sigma + \map {e_2} \sigma&
    \map {c \cdot x} \sigma      &= \map {c} \sigma \cdot \map {x} \sigma\\
    \map k \sigma                &= k&
    \map x \sigma                &= x
  \end{align*}}
\end{frame}

\subsection{Linear Programs}
\begin{frame}{Linear Program}
  We find the solution of the system of rational $\land$-equations by
  constructing a linear program:
  \begin{align*}
    \lpt{\syseq} &= \max \underline{1}^\transp \mathbf{x},\, x_i \in \X\\
      &s.t. \,\, \bigcup \{\lpt{x = e} \,|\, x = e \in \syseq\}\\
    \lpt{x = e_1 \minop e_2} &= \{x \le e_1, x \le e_2\}\\
    \lpt{x = \lpop{A}{c}{\vec{e}}} &= \{x \le c^\transp \vec{y}\} \cup
      \{A \vec{y} \le \vec{e}\}\\
    \lpt{x = e} &= \{x \le e\},\,\,\, e = c \,|\, x \,|\, e_1 + e_2 \,|\, c \cdot x
  \end{align*}
\end{frame}

\subsection{Strategy Iteration}
\begin{frame}{Strategy Iteration}
  We define an initial strategy $\sigma_{init}$ for the system of equations and
  then improve our strategy until we find a solution for the system.\pause
  
  \textbf{Improvement operator:}
  {\small$$
    P_\maxop^\mathsf{eager}(\sigma, \rho)(e_1 \maxop e_2) =
      \begin{cases}
        e_1 & \text{ if } [e_1]_\rho > [e_2]_\rho\\
        e_2 & \text{ if } [e_1]_\rho < [e_2]_\rho\\
        \sigma(e_1 \maxop e_2) & \text{ if } [e_1]_\rho = [e_2]_\rho
      \end{cases}
  $$}\pause
  \textbf{Algorithm:}

{\small
\begin{algorithmic}
  \Function{Solve}{$\syseq, \sigma_{\mathrm{init}}, \rho_{\mathrm{init}}$}
  \State $\sigma \gets \sigma_{\mathrm{init}}$
  \State $\rho \gets \rho_{\mathrm{init}}$
  \While{$\rho \notin \mathbf{Solutions}(\syseq)$}
    \State $\sigma \gets P_\maxop^\mathsf{eager}(\sigma, \rho)$
    \State $\rho \gets \lpt{[\syseq]_\sigma}$
  \EndWhile
  \State \Return $\rho$
  \EndFunction
\end{algorithmic}}
\end{frame}

\section{Example}
\begin{frame}{Example}
\begin{align*}
  x_1 &= {4 \over 5} \cdot x_1 + x_2 \lor 2 \lor -\infty\\
  x_2 &= x_2 + 1 \land 100 \lor x_1 \lor -\infty
\end{align*}
\end{frame}

\begin{frame}{Initial Strategy}
\begin{align*}
  x_1 &= {\color{gray}{4 \over 5} \cdot x_1 + x_2 \lor 2 \lor} -\infty\\
  x_2 &= {\color{gray}x_2 + 1 \land 100 \lor x_1 \lor} -\infty
\end{align*}
Results in: \hspace{2em} $x_1 = -\infty$ \hspace{2em} $x_2 = -\infty$
\end{frame}

\begin{frame}{Next Strategy}
\begin{align*}
  x_1 &= {\color{gray}{4 \over 5} \cdot x_1 + x_2 \lor} 2 {\color{gray}\lor -\infty}\\
  x_2 &= {\color{gray}x_2 + 1 \land 100 \lor x_1 \lor} -\infty
\end{align*}
Results in: \hspace{2em} $x_1 = 2$ \hspace{2em} $x_2 = -\infty$
\end{frame}

\begin{frame}{Next Strategy}
\begin{align*}
  x_1 &= {\color{gray}{4 \over 5} \cdot x_1 + x_2 \lor} 2 {\color{gray}\lor -\infty}\\
  x_2 &= {\color{gray}x_2 + 1 \land 100 \lor} x_1 {\color{gray}\lor -\infty}
\end{align*}
Results in: \hspace{2em} $x_1 = 2$ \hspace{2em} $x_2 = 2$
\end{frame}

\begin{frame}{Next Strategy}
\begin{align*}
  x_1 &= {4 \over 5} \cdot x_1 + x_2 {\color{gray}\lor 2 \lor -\infty}\\
  x_2 &= x_2 + 1 \land 100 {\color{gray}\lor x_1 \lor -\infty}
\end{align*}
Results in: \hspace{2em} $x_1 = 2$ \hspace{2em} $x_2 = 2$
\end{frame}

\begin{frame}{Linear Problem}
\begin{align*}
  x_1 &= {4 \over 5} \cdot x_1 + x_2\\
  x_2 &= x_2 + 1 \land 100
\end{align*}\pause

Is translated to:
$$\max x_1 + x_2$$
s.t.
\begin{align*}
  {1\over5} \cdot x_1 - x_2 &\le 0\\
  x_2 &\le 100\\
\end{align*}\pause
With solution: \hspace{2em} $x_1 = 500$ \hspace{2em} $x_2 = 100$
\end{frame}

\section{Tool}
\begin{frame}{Advertisement}
  I built a tool for this: RESIS :)
  \vspace{1em}
  Please feel free to check it out at:\\
  \url{https://bitbucket.org/hoheinzollern/resis}
  \vspace{1em}
  
  And ask me questions if you are interested!
\end{frame}
\end{document}