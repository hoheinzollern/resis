\documentclass {article}

\usepackage{amsmath,amsfonts,amssymb,stmaryrd}
\usepackage{algpseudocode,algorithm}
\usepackage{hyperref}

\newcommand{\toolname}{RESIS}
\newcommand{\rationals}{\mathbb{Q}}
\newcommand{\rationalspp}{\mathbb{Q}^+}
\newcommand{\lpop}[3]{\mathrm{LP}_{#1,#2}(#3)}
\newcommand{\maxop}{\vee}
\newcommand{\minop}{\wedge}
\newcommand{\X}{\mathbf{X}}
\newcommand{\E}{\mathbf{E}}
%\newcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\syseq}{\varepsilon}
\newcommand{\transp}{\intercal}
\newcommand{\lpt}[1]{{\llbracket#1\rrbracket}_\mathrm{LP}}
\newcommand{\map}[2]{{[#1]}_{#2}}

\newcommand{\seclbl}[1]{\label{sec:#1}}
\newcommand{\secref}[1]{Section~\ref{sec:#1}}

\newtheorem{example}{Example}

\newcommand{\mytitle}{\toolname, a Solver for Rational Equations}
\newcommand{\myauthor}{Alessandro Bruni}

\hypersetup{
    bookmarks=true,         % show bookmarks bar?
    unicode=false,          % non-Latin characters in Acrobat’s bookmarks
    pdftoolbar=true,        % show Acrobat’s toolbar?
    pdfmenubar=true,        % show Acrobat’s menu?
    pdffitwindow=false,     % window fit to page when opened
    pdfstartview={FitH},    % fits the width of the page to the window
    pdftitle={\mytitle},    % title
    pdfauthor={\myauthor},  % author
    pdfsubject={Subject},   % subject of the document
    pdfcreator={Creator},   % creator of the document
    pdfproducer={Producer}, % producer of the document
    pdfkeywords={keyword1} {key2} {key3}, % list of keywords
    pdfnewwindow=true,      % links in new window
    colorlinks=true,        % false: boxed links; true: colored links
    linkcolor=black,        % color of internal links (change box color with linkbordercolor)
    citecolor=black,        % color of links to bibliography
    filecolor=black,        % color of file links
    urlcolor=cyan           % color of external links
}

\title{\mytitle}
\author{\myauthor}

\begin{document}
\maketitle

\section{Introduction}
\seclbl{intro}

This document describes \toolname, a solver for systems of rational
LP-equations.

\subsection{Problem statement}
\label{statement}

Rational LP-equations are equations of the form $x = e_\X$ where $x \in \X$ is
a variable and $e_\X \in \E$ is an expression of the form:

\begin{align*}
  e &= x,\,\, x \in \X &
  e &= c,\,\, c \in \rationalspp\\
  e &= e_1 + e_2 &
  e &= c \times x,\,\, c \in \rationals_{\ge 0},\,\, x \in \X\\
  e &= e_1 \maxop e_2 &
  e &= e_1 \minop e_2\\
  e &= \lpop{A}{c}{e_1,\dots,e_n}
\end{align*}
\\
Where $\rationalspp = \rationals \cup \{-\infty, \infty\}$, $+$ is the sum over
$\rationals$ extended to $\rationalspp$ so that $x + -\infty = -\infty + x =
-\infty$ for all $x \in \rationalspp$, $x + \infty = \infty + x = \infty$ for
$x \in \rationals \cup \{\infty\}$, and $\times$ is the product of a positive
constant and a variable. $\maxop$ is defined as the maximum between the two
sub-expressions and $\minop$ is defined as the minimum between the two
subexpressions.

The LP-operator $\lpop{A}{c}{e_1,\dots,e_n}$ has the following meaning:
$$\lpop{A}{c}{e_1,\dots,e_n} = \max\{ c^\transp \mathbf{x} \mid A \mathbf{x}
\leq \mathbf{e}\}$$

A system of rational LP-equations is defined as follows:

$$\syseq = \{x = e_\X | x \in \X\}$$

\subsection{Solution}
\label{solution}

The solution of a system $\syseq$ is an assignment $\rho: \X \to \rationalspp$
that satisfies all the equations in $\syseq$.

According to what is specified in \cite{Gawlitza2011}, a solution can be found
through strategy iteration, by means of treating a system of rational
min-LP-equations as a linear problem where we want to maximize the value of
$\underline{1}^\transp \mathbf{x}$ under the constraints $\{x \le e \,|\, x = e
\in \syseq\}$.

A system of min-LP-equations is a system of LP-equations without max operators
in the expressions. Such system can be obtained from a system of LP-equations
by making a choice for each $\maxop$-expression. We define a strategy $\sigma$
as a set of choices for the $\maxop$-expressions of a system of LP-equations:

$$\sigma = \left\{e_1 \maxop e_2 \to e_i \,|\, i \in \{1,2\},\, e_1 \maxop e_2
\in \syseq \right\}$$

\begin{example}
Consider the following system of equations:

$$\syseq = \left\{ x_1 = -10 \maxop 0.75 \times x_2 + 1,\, x_2 = 2 \times x_1,
\, x_3 = x_3 + x_1 - 1 \maxop 0\right\}$$

A possible strategy for $\syseq$ is $\sigma = \{-10 \maxop 0.75 \times x_2 + 1
\to -10,\, x_3 + x_1 - 1 \maxop 0 \to x_2 + x_1 - 1\}$

\end{example}

We can derive a system of min-LP-equations from a system of LP-equations by
mapping the expressions with our strategy:

\begin{align*}
  \map \syseq \sigma           &= \{x = \map e \sigma \,|\, x = e \in \syseq\}&
  \map {e_1 \maxop e_2} \sigma &=
    \begin{cases}
      \map {e_1} \sigma & \text{iff } \sigma(e_1 \maxop e_2) = e_1 \\
      \map {e_2} \sigma & \text{iff } \sigma(e_1 \maxop e_2) = e_2
    \end{cases}\\
  \map {e_1 \minop e_2} \sigma &= \map {e_1} \sigma \minop \map {e_2} \sigma&
  \map {\lpop{A}{c}{\vec{e}}} \sigma &= \lpop{A}{c}{\map {\vec{e}} \sigma} \\
  \map {e_1 + e_2} \sigma      &= \map {e_1} \sigma + \map {e_2} \sigma&
  \map {c \times x} \sigma     &= \map {c} \sigma \times \map {x} \sigma\\
  \map c \sigma                &= c&
  \map x \sigma                &= x
\end{align*}

\subsubsection{Linear Program}

We can solve a system of min-LP-equations $\syseq$ by solving a linear program
whose size is linear w.r.t. the size of $\syseq$. For simplicity, we solve
min-LP-systems in normal form, i.e. systems of equations where $\{\minop,
\maxop, \mathrm{LP}\}$-expressions appear only at the first level. Note that we
can convert any min-LP-system in normal form by introducing fresh variables and
assign them to the nested sub-expressions.

Each equation of $\syseq$ is mapped to the following sets of constraints:
\begin{align*}
  \lpt{\syseq} &= \max \underline{1}^\transp \mathbf{x},\, x_i \in \X\\
    &s.t. \,\, \bigcup \{\lpt{x = e} \,|\, x = e \in \syseq\}\\
  \lpt{x = e_1 \minop e_2} &= \{x \le e_1, x \le e_2\}\\
  \lpt{x = \lpop{A}{c}{\vec{e}}} &= \{x \le c^\transp \vec{y}\} \cup
    \{A \vec{y} \le \vec{e}\}\\
  \lpt{x = e} &= \{x \le e\},\,\,\, e = c \text{ or } e = x \text{ or } 
    e = e_1 + e_2 \text{ or } e = c \times x
\end{align*}

$\lpt{\syseq}$ will be either infeasible, unbounded or feasible and bounded. If
$\lpt{\syseq}$ is infeasible, $\syseq$ has no finite solution. If it is
feasible, then the assignment optimal solution $\rho$ for the linear problem is
also an optimal solution for $\syseq$. If it is unbounded, some of the
variables will be assigned $\infty$.

\subsubsection{Strategy Iteration}
\label{strategy-iter}

As stated at the beginning of this section, a system $\syseq$ of rational
LP-equations can be solved through strategy iteration: we define an initial
strategy $\sigma_0$ for each $\maxop$-expression in $\syseq$, we solve the
linear problem $\lpt{[\syseq]_{\sigma_0}}$, and then we improve the strategy
until we reach an optimal solution.

Since all our operators are monotonic, we can define a strategy improvement
operator which also preserves monotonicity. Such operator is defined greedly as:

$$
  P_\maxop^\mathsf{eager}(\sigma, \rho)(e_1 \maxop e_2) =
    \begin{cases}
      e_1 & \text{ if } [e_1]_\rho > [e_2]_\rho\\
      e_2 & \text{ if } [e_1]_\rho < [e_2]_\rho\\
      \sigma(e_1 \maxop e_2) & \text{ if } [e_1]_\rho = [e_2]_\rho
    \end{cases}
$$

Our algorithm for computing the optimal solution is defined as follows.

\begin{algorithm}
\caption{Strategy iteration algorithm}
\label{alg:strategy-iteration}
\begin{algorithmic}
  \Function{Solve}{$\syseq, \sigma_{\mathrm{init}}, \rho_{\mathrm{init}}$}
  \State $\sigma \gets \sigma_{\mathrm{init}}$
  \State $\rho \gets \rho_{\mathrm{init}}$
  \While{$\rho \notin \mathbf{Solutions}(\syseq)$}
    \State $\sigma \gets P_\maxop^\mathsf{eager}(\sigma, \rho)$
    \State $\rho \gets \lpt{[\syseq]_\sigma}$
  \EndWhile
  \State \Return $\rho$
  \EndFunction
\end{algorithmic}
\end{algorithm}

\section{Technical Details}
\seclbl{details}

\toolname~combines the techniques of linear programming and strategy iteration,
as described in \cite{Gawlitza2011} and specified in the previous section, to
find a solution for the system of equations.

\toolname~is freely available at
\url{https://bitbucket.org/hoheinzollern/resis} and is implemented in C++ using
the Coin-OR Linear Program Solver (CLP) library to solve linear programs.

%\subsection{Design choices}
%\label{design}

\subsection{Usage}
\label{usage}

\toolname~can be either used as a library or as a standalone application. In
this document we will describe the latter.

\subsubsection{Requirements}

In order to install the software you need to have installed in your system the
following dependencies:

\begin{enumerate}
  \item Flex/Bison
  \item A suitable C++ compiler (GCC/Clang should work)
  \item CMake build system
  \item Coin-OR CLP Linear Program Solver, available at 
    \url{http://www.coin-or.org/Clp/}
  \item Git (to obtain the latest version from the repository)
\end{enumerate}

Each of these packages should be available through the package manager of your
distribution, except CLP, which needs to be downloaded and installed manually.
Once they are all setup you can proceed with the installation of \toolname.

\subsubsection{Installation}

To install the package in an Unix environment (Linux/Mac OS X should work)
you need to download and compile the latest version of the software:

\begin{verbatim}
$ git clone https://bitbucket.org/hoheinzollern/resis.git
$ cd resis
$ mkdir build
$ cd build
$ cmake ..
\end{verbatim}
At this point you should check that everything went fine and all the dependencies are recognized.
\begin{verbatim}
$ make && sudo make install
\end{verbatim}

Once \toolname~is set up you can call the executable with a LP-system:

\begin{verbatim}
$ resis < problem.resis
\end{verbatim}

In the directory \texttt{/examples/} of the project there are some sample
programs that you can try out.

\section{Getting started with \toolname}
\label{tutorial}

\toolname~solves systems of rational LP-equations as described in this
document. Additionaly to equations, one can specify a set of templates that can
be used systematically with the LP operators. It is able to parse a simple
language as described in Section~\ref{language}.

\subsection{The Language}
\label{language}

A \toolname~program has the following structure:

\begin{verbatim}
templates
# templates go here
equations
# equations go here
\end{verbatim}

The template section is optional and can be omitted, while the equations
section must be present.

\subsubsection{Template Section}

Templates may be either matrices or vectors. Template identifiers all start
with a letter and are possibly followed by any sequence of letters, digits and
the underscore character (\_). Matrix identifiers must start with an uppercase
letter, while vector identifiers must start with a lowercase letter. Each
template declaration is basically an assignment of the form:
\begin{verbatim}
A = [1, 0; \
     0, 1]
\end{verbatim}
for matrices and:
\begin{verbatim}
c = [1, 1]
\end{verbatim}
for vectors.

Matrices and vectors can also be extended by means of the \texttt{:}
operator. The \texttt{:} operator concatenates two matrices or vectors. For
example:
\begin{verbatim}
T1 = A : [1, 1]
\end{verbatim}
Assigns to \texttt{T1} the value \texttt{[1, 0; 0, 1; 1, 1]}.

Each declaration must be followed by a newline character. Spaces of any type
don't matter, so you can use them freely. To continue a declaration across lines
you can just use a backslash character (\textbackslash).

Templates may be referenced by LP-operators, and this mechanism provides a way
to easily share a single template matrix/vector among different LP calls.

\subsubsection{Equations Section}

The rational LP-system is expressed in terms of equations between a variable
and an expression. Expressions can use the operators defined in
Section~\secref{intro}, and the operators are all left-associative and follow
this order of precedence: $\minop, \maxop, \times, +, \lpop{A}{c}{args}$.
Parentheses may be used to diambiguate the precedence, if needed.

An example program using the operators is the following:
\begin{verbatim}
equations
x1 = -10 \/ 0.75 * x2 + 1
x2 = 2 * x1
x3 = x3 + x1 - 1 \/ 0
\end{verbatim}
We can see here that the $\times$ operator is represented by \texttt{*}, the
min and max operators are represented by \texttt{\char`\\/} and
\texttt{/\char`\\}, repsectively, and that the $-$ sign can be used as a
shorthand for $\dots + (-n)$, where n is a rational number. Also, \texttt{oo}
and \texttt{-oo} may be used to specify plus and minus infinity as constant in
the equations.

\paragraph{The LP-operator} takes the following form: \texttt{LP<A,c>(args)},
where A and c are a matrix and a vector defined in the templates section, or
defined in place. For example:
\begin{verbatim}
templates
A = [1,0;0,1]
c = [1,1]
equations
x = LP<A, c>(x1, x2)
x1 = x1 /\ x2 \/ x + 1
x2 = 0
\end{verbatim}
Defines a set of equation using $\minop$, $\maxop$ and LP operators.

An alternative formulation of the same LP operator with inline templates would
be the following:
\begin{verbatim}
x = LP<[1,0;0,1],[1,1]>(x1, x2)
\end{verbatim}

%\subsection{Example}
%\label{example}

% \section{Programming Interface}
% \label{prog-interface}

\bibliographystyle{plain}
\bibliography{bibliography}

\end{document}
