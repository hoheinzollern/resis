/**
 * @file
 * @author Alessandro Bruni <alessandro.bruni@gmail.com>
 *
 * @section DESCRIPTION
 * 
 * Data structure and functions for representing and creating the AST
 * of a system of rational equations.
 *
 * @section LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXPRESSION_H
#define EXPRESSION_H

// Uncomment this to show debug messages
//#define DEBUG

#define LEFT 0
#define RIGHT 1

#include <stdint.h>
#include <stdio.h>

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <map>

using namespace std;

namespace Resis {

/**
 * Supported expression types
 */
typedef enum {
  VARIABLE,
  CONSTANT,
  EQUATION,
  MIN_EXP,
  MAX_EXP,
  SUM_EXP,
  MUL_EXP,
  LP_OP
} ExpType;

class Expression;
class Equation;
class System;

const int UNDEFINED = -1;

typedef pair<int, Equation *> IntEqPair;
typedef vector<int> IntVector;
typedef vector<double> DblVector;
typedef vector<DblVector> DblMatrix;
typedef vector<Expression *> ExpVector;
typedef vector<Equation *> EqVector;
typedef map<int, Equation *> EqMap;
typedef vector<EqMap> EqMatrix;

/**
 * In this solver we try to avoid fancy OO constructs, we rather apply
 * a pattern matching approach, so Expression serves as a base class
 * with only the subtype specified.
 */
class Expression {
public:
  ExpType type;

  Expression(ExpType type);
  virtual double value() = 0;
  
  virtual ~Expression();
};

/**
 * Represents a variable, with all the necessary information for the
 * LP-solver and the user.
 */
class ExpVariable : public Expression {
  friend class System;
public:
  bool temp;     /// True if variable is temporary
  uint32_t id;   /// Variable identifier
  int lpid;      /// LP-index
  string name;   /// Variable name
  double _value;  /// Current value (NaN if not bound)

  bool isFree();
  bool isBound();
  void clear();
  virtual double value();

private:
  /**
   * Constructor, takes a variable identifier and a name string. Do
   * not use this directly, use System::createVariable(name) instead.
   */
  ExpVariable(uint32_t id, string &name, bool temp);
};

/**
 * As simple as a constant may be
 */
class ExpConstant : public Expression {
  friend class System;
public:
  /// Current value
  double _value;
  
  virtual double value();

private:
  ExpConstant(double value);
};

/**
 * Represents an equation, in the form:
 *  x = exp
 * where x is a variable and exp is an arbitrary expression.
 */
class Equation : public Expression {
  friend class System;
public:
  /// The variable
  ExpVariable *var;
  /// The expression
  Expression *exp;

  virtual double value();

private:
  Equation(ExpVariable *var, Expression *exp);
};

/**
 * Represents an arbitrary n-ary expression. Possible type values are:
 * MIN_EXP, MAX_EXP, SUM_EXP, MUL_EXP
 */
class ExpNary : public Expression {
  friend class System;
  friend class ExpLP;
  friend class MinExp;
  friend class MaxExp;
  friend class SumExp;
  friend class MulExp;
public:
  ExpVector args;

private:
  /// Binary constructor. Flattens sub expressions if they are of the
  /// same type of the one that is being constructed.
  ExpNary(ExpType type, Expression *left, Expression *right);
  /// Nary constructor. Basically copies the type and the arguments
  ExpNary(ExpType type, ExpVector *args);
};

class MinExp : public ExpNary {
  friend class System;
public:
  virtual double value();
private:
  MinExp(Expression *left, Expression *right);
};

class MaxExp : public ExpNary {
  friend class System;
public:
  virtual double value();
private:
  MaxExp(Expression *left, Expression *right);
};

class SumExp : public ExpNary {
  friend class System;
public:
  virtual double value();
private:
  SumExp(Expression *left, Expression *right);
};

class MulExp : public ExpNary {
  friend class System;
public:
  virtual double value();
private:
  MulExp(Expression *left, Expression *right);
};

/**
 * Represents an LP operator, A and c being the two matrices.
 */
class ExpLP : public ExpNary {
  friend class System;
public:
  /// The constraint matrix
  DblMatrix A;
  /// The objective function
  DblVector c;
  /// LP-index
  int lpid;
  
  /// Number of new columns in the LP problem (fresh variables)
  int cols();
  /// Number of constraints in the LP problem
  int rows();

  virtual double value();

private:
  ExpLP(DblMatrix *A, DblVector *c, ExpVector *args);
};

typedef map<string, ExpVariable *> VarMap;

/**
 * Represents a system of equations.
 */
class System {
public:
  /// Vector of the equations of the system
  EqVector equations;

  /// Maps each variable name to its reference
  VarMap variables;

  /// Returns the size of the system, namely the number of equations
  int size();

  /// Adds a new equation to the system
  void createEquation(ExpVariable *v, Expression *exp);

  /// Creates a new variable and adds it to the variable map
  ExpVariable *createVariable(string &name);

  /// Creates a new temporary variable
  ExpVariable *createFreshVariable();

  /**
   * Normalizes the system of equations
   *
   * Puts the system in normal form, creating new temporary variables
   * to flatten the equations to just one level.
   */
  void normalize(); 

  /// Solves the system
  void solve();

  /// Creates a sum expression
  ExpNary *createSum(Expression *left, Expression *right);
  
  /// Creates a product expression
  ExpNary *createMul(Expression *left, Expression *right);
  
  /// Creates a /\ expression
  ExpNary *createMin(Expression *left, Expression *right);
  
  /// Creates a \/ expression
  ExpNary *createMax(Expression *left, Expression *right);
  
  /// Creates an LP operator
  ExpLP *createLP(DblMatrix *A, DblVector *c, ExpVector *args);
  
  /// Creates a rational constant
  ExpConstant *createConstant(double val);

  /// Default constructor
  System();
private:
  int next_free;

  Expression *normalizeExpr(Expression *exp, int depth);
};

/// Prints an arbitrary expression
ostream &operator<<(ostream &stream, Expression &exp);

/// Prints a system of equations
ostream &operator<<(ostream &stream, System &sys);

}

#endif
