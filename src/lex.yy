%{
/**
 * @file
 * @author Alessandro Bruni <alessandro.bruni@gmail.com>
 *
 * @section DESCRIPTION
 * 
 * Tokenizer for parsing systems of rational equations
 *
 * @section LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "resis.hpp"
#include "parser.hpp"

using namespace Resis;

%}

%option outfile="lex.yy.cpp" header-file="lex.yy.hpp"

%option reentrant noyywrap never-interactive nounistd
%option bison-bridge

digit	[0-9]
letter	[a-zA-Z_]
ucletter [A-Z]
lcletter [a-z]
comment "#"([^\n]*)

ccident	{ucletter}({letter}|{digit})*
cbident {lcletter}({letter}|{digit})*
integer {digit}+
exponent [eE][+-]?{integer}
number [\-]?{integer}("."{integer})?{exponent}?

%%

"oo"      { return INF;      }
"inf"     { return INF;      }
"-oo"     { return NINF;     }
"-inf"    { return NINF;     }
"+"       { return PLUS;     }
"*"       { return TIMES;    }
"/\\"     { return MIN;      }
"\\/"     { return MAX;      }
"LP"      { return LPOP;     }
"="       { return EQ;       }
"("       { return LPAREN;   }
")"       { return RPAREN;   }
"\["      { return LBRACKET; }
"\]"      { return RBRACKET; }
"<"       { return LT;       }
">"       { return GT;       }
","       { return CM;       }
";"       { return SCOL;     }
":"       { return COL;      }
"templates" { return TEMPLATES; }
"equations" { return EQUATIONS; }
{ccident} { yylval->name = strdup(yytext);
	    return CCIDENT;    }
{cbident} { yylval->name = strdup(yytext);
	    return CBIDENT;    }
{number}  { yylval->name = strdup(yytext);
	    return CONST;    }
"-"       { return MINUS;    }
[\n]      { return ENDL;     }
[ \t\r]   /* skip whitespace */
{comment} /* skip comments */
"\\\n"    /* ignore this sequence (for multiline equations) */
.         { printf("Unknown character [%c]\n", yytext[0]);
            return UNKNOWN;  }

%%

