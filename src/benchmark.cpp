#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include "resis.hpp"

using namespace std;
using namespace Resis;

const int N_VARIABLES = 10000;
const int N_EQUATIONS = 5;
System sys;
vector<ExpVariable*> variables(N_VARIABLES);

Expression *randVar() {
  return variables[rand() % N_VARIABLES];
}

int coin() {
  return rand() % 2;
}

double uni() {
	return (double)rand() / (double)RAND_MAX;
}

Expression *randConst() {
	double x = coin() * uni();
	return sys.createConstant(x);
}

Expression *makeRndExp(int depth = 0) {
	if (depth >= 5) {
		return coin() ? randVar() : randConst();
	} else {
		double r = uni();
		if (r < 0.15)
			return randVar();
		else if (r < 0.30)
			return randConst();
		else if (r < 0.50)
			return sys.createMul(randConst(), randVar());
		else if (r < 0.60)
			return sys.createSum(makeRndExp(depth+1), makeRndExp(depth+1));
		else if (r < 0.80)
			return sys.createMin(makeRndExp(depth+1), makeRndExp(depth+1));
		else
			return sys.createMax(makeRndExp(depth+1), makeRndExp(depth+1));
	}
}

int main(int argc, char **argv) {
	srand(time(NULL));
	for (int i = 0; i < N_VARIABLES; i++) {
		stringstream name;
		name << "x" << i;
		string name_s = name.str();
		variables[i] = sys.createVariable(name_s);
	}
	
	for (int i = 0; i < N_VARIABLES; i++) {
		sys.createEquation(variables[i], makeRndExp());
	}
    
    sys.normalize();
	cout << sys;

	sys.solve();
	return 0;
}