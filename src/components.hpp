/**
 * @file
 * @author Alessandro Bruni <alessandro.bruni@gmail.com>
 *
 * @section DESCRIPTION
 * 
 * This module builds the strongly connected components which can then
 * be solved separately, in order to build smaller subproblems.
 *
 * @section LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP

#include "resis.hpp"

#include <iostream>
#include <utility>
#include <algorithm>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/visitors.hpp>

using namespace std;
using namespace boost;

namespace Resis {

typedef adjacency_list<vecS, vecS, directedS> Graph;

void buildComponents(System &sys, EqMatrix &components);

}

#endif
