/**
 * @file
 * @author Alessandro Bruni <alessandro.bruni@gmail.com>
 *
 * @section DESCRIPTION
 * 
 * Main routine, hopefully in the future it will accept parameters and
 * stuff.
 *
 * @section LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <unistd.h>
#include "resis.hpp"
 
#include "parser.hpp"
#include "lex.yy.hpp"

using namespace std;
using namespace Resis;

int yyparse(yyscan_t, System*);

int main(int argc, char **argv) {
  if (isatty(STDIN_FILENO)) {
    cout << "Resis - a solver for rational equations" << endl
	 << "Usage: " << argv[0] << " < filename" << endl;
  } else {
    // Step 0: initialize
    System sys;
    yyscan_t scanner;

    yylex_init(&scanner);

    // Step 1: parse
    yyparse(scanner, &sys);

    yylex_destroy(scanner);

    //  Step 1.5: normalize
    sys.normalize();

    cerr << sys;

    // Step 2: solve
    sys.solve();
  }
  return 0;
}
