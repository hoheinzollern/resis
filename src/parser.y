%{
/**
 * @file
 * @author Alessandro Bruni <alessandro.bruni@gmail.com>
 *
 * @section DESCRIPTION
 * 
 * Parses systems of rational equations
 *
 * @section LICENSE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "resis.hpp"
#include <math.h>
#include <assert.h>

%}

%output  "parser.cpp"
%defines "parser.hpp"

%code requires {

#include <map>
#include <string>
#include <algorithm>
  using namespace std;
  using namespace Resis;

typedef union YYSTYPE{
  Expression *expression;
  ExpVariable *variable;
  ExpConstant *constant;
  ExpNary *expnary;
  ExpLP *explp;

  ExpVector *exparray;
  struct {
    DblVector *v;
    bool tmp;
  } vector;
  DblVector *tmpvector;
  struct {
    DblMatrix *m;
    bool tmp;
  } matrix;
  DblMatrix *tmpmatrix;
  char *name;
  double value;
} YYSTYPE;

#ifndef YY_TYPEDEF_YY_SCANNER_T
#define YY_TYPEDEF_YY_SCANNER_T
  typedef void* yyscan_t;
#endif
  
  int yylex(YYSTYPE * yylval_param ,yyscan_t yyscanner);
  int yyerror(yyscan_t scanner, System *equations, const char *msg);

  typedef map<string, DblMatrix *> MatrixMap;
  typedef map<string, DblVector *> VectorMap;

  extern MatrixMap matrices;
  extern VectorMap vectors;
  extern int current_line;

}

%define api.pure
%lex-param   { yyscan_t scanner }
%parse-param { yyscan_t scanner }
%parse-param { System *sys }

%left '+' PLUS
%left '*' TIMES
%left ':' COL
%left ';' SCOL

%token PLUS
%token MINUS
%token TIMES
%token MIN
%token MAX
%token LPAREN
%token RPAREN
%token LBRACKET
%token RBRACKET
%token EQ
%token CM
%token GT
%token LPOP
%token LT
%token SCOL
%token COL
%token TEMPLATES
%token EQUATIONS
%token <name> CCIDENT
%token <name> CBIDENT
%token <name> CONST
%token INF
%token NINF
%token ENDL
%token UNKNOWN

%type <expression> max_expr
%type <expression> min_expr
%type <expression> plus_expr
%type <expression> mult_expr
%type <variable> ident
%type <constant> num
%type <explp> lp_expr
%type <vector> vector_expr
%type <matrix> matrix_expr
%type <tmpmatrix> vector_list
%type <tmpmatrix> vector_list_
%type <tmpvector> dbl_vals
%type <tmpvector> dbl_vals_
%type <value> dbl_val
%type <exparray> args
%type <exparray> args_
%type <name> ccident
%type <name> cbident

%%

input
: TEMPLATES endl templates EQUATIONS endl equations { }
| EQUATIONS endl equations { };

templates
: matr_decl endl templates { }
| vect_decl endl templates { }
| endl templates { }
| { }
;

equations
: equations eq endl { }
| equations endl { }
| eq endl { }
;

matr_decl
: ccident EQ matrix_expr { matrices[$1] = $3.m; };

vect_decl
: cbident EQ vector_expr { vectors[$1] = $3.v; };

eq
: ident EQ max_expr { sys->createEquation($1, $3); }
;

max_expr
: min_expr MAX max_expr  { $$ = sys->createMax($1, $3); }
| min_expr               { $$ = $1; }
;

min_expr
: plus_expr MIN min_expr { $$ = sys->createMin($1, $3); }
| plus_expr              { $$ = $1; }
;

plus_expr
: mult_expr PLUS plus_expr     { $$ = sys->createSum($1, $3); }
| mult_expr MINUS plus_expr    {
  assert($3->type == CONSTANT);
  ExpConstant *c = (ExpConstant *)$3;
  c->_value = -c->_value;
  $$ = sys->createSum($1, c);
 }
| mult_expr                    { $$ = $1; }
;

mult_expr
: num TIMES ident        { $$ = sys->createMul($1, $3); }
| ident                  { $$ = $1; }
| num                    { $$ = $1; }
| LPAREN max_expr RPAREN { $$ = $2; }
| lp_expr                { $$ = $1; }
;

lp_expr
: LPOP LT matrix_expr CM vector_expr GT LPAREN args RPAREN {
  if ($3.m->begin()->size() != $5.v->size()) {
    yyerror(scanner, sys, "Matrix column size does not match vector size");
  }
  if ($3.m->size() != $8->size()) {
    yyerror(scanner, sys, "Matrix row size does not match the number of arguments");
  }
  $$ = sys->createLP($3.m, $5.v, $8);
};

matrix_expr
: ccident {
  MatrixMap::iterator mit = matrices.find($1);
  if (mit == matrices.end()) {
    yyerror(scanner, sys, "Matrix identifier not yet declared");
  }
  $$.m = mit->second;
  $$.tmp = false;
}
| LBRACKET vector_list RBRACKET { $$.m = $2; $$.tmp = true; }
| matrix_expr COL matrix_expr {
  if ($1.m->begin()->size() != $3.m->begin()->size()) {
    yyerror(scanner, sys, "Extending matrices of different column size");
  }
  if ($1.tmp) {
    $$.m = $1.m;
  } else {
    $$.m = new DblMatrix(*($1.m));
  }
  $$.tmp = true;
  $$.m->insert($$.m->end(), $3.m->begin(), $3.m->end());
  if ($3.tmp) {
    delete $3.m;
  }
};

vector_expr
: cbident {
  VectorMap::iterator vit = vectors.find($1);
  if (vit == vectors.end()) {
    yyerror(scanner, sys, "Vector identifier not yet declared");
  }
  $$.v = vit->second;
  $$.tmp = false;
}
| LBRACKET dbl_vals RBRACKET { $$.v = $2; $$.tmp = true; }
| vector_expr COL vector_expr {
  if ($1.tmp) {
    $$.v = $1.v;
  } else {
    $$.v = new DblVector(*($1.v));
  }
  $$.v->insert($$.v->end(), $3.v->begin(), $3.v->end());
  if ($3.tmp) {
    delete $3.v;
  }
};

vector_list : vector_list_ {
  $$ = $1;
  reverse($$->begin(), $$->end());
}

vector_list_
: dbl_vals SCOL vector_list_ {
  $$ = $3;
  if ($$->begin()->size() != $1->size()) {
    yyerror(scanner, sys, "Line length mismatch in matrix");
  }
  $$->push_back(*$1);
}
| dbl_vals {
  $$ = new DblMatrix();
  $$->push_back(*$1);
};

dbl_vals
: dbl_vals_ {
  $$ = $1;
  reverse($$->begin(), $$->end());
};

dbl_vals_
: dbl_val CM dbl_vals_ {
  $$ = $3;
  $$->push_back($1);
}
| dbl_val {
  $$ = new DblVector();
  $$->push_back($1);
};

dbl_val
: CONST {
  sscanf(yylval.name, "%lf", &$$);
};

args
: args_ {
  $$ = $1;
  reverse($$->begin(), $$->end());
};

args_
: max_expr CM args_ {
  $$ = $3;
  $$->push_back($1);
}
| max_expr {
  $$ = new ExpVector();
  $$->push_back($1);
};

ident
: ccident { string name = $1;
   $$ = sys->createVariable(name); }
| cbident { string name = $1;
   $$ = sys->createVariable(name); }
;

ccident : CCIDENT { $$ = yylval.name; }
cbident : CBIDENT { $$ = yylval.name; }

num
: dbl_val { $$ = sys->createConstant($1); }
| INF   { $$ = sys->createConstant(INFINITY); }
| NINF  { $$ = sys->createConstant(-INFINITY); }
;

endl
: ENDL { current_line++; }
| ENDL endl { current_line++; }

%%

MatrixMap matrices;
VectorMap vectors;
int current_line = 1;

int yyerror (yyscan_t scanner, System *sys, const char *msg) {
  cerr << msg << endl << "at line " << current_line << endl;
  exit(1);
  return 1;
}
