/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "components.hpp"
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

 namespace Resis {

/**
 * Builds the set of dependencies for an equation, finding all the
 * variables appearing on its right side.
 */
void buildDeps(Graph &dependencies, ExpVariable *var, Expression *exp) {
  switch (exp->type) {
  case VARIABLE:
    add_edge(((ExpVariable *)exp)->id, var->id, dependencies);
    break;
  case MAX_EXP:
  case MIN_EXP:
  case SUM_EXP:
  case MUL_EXP:
  case LP_OP: {
    ExpNary &expn = *((ExpNary *)exp);
    for (ExpVector::iterator it = expn.args.begin(); it != expn.args.end(); ++it) {
      buildDeps(dependencies, var, *it);
    }
    break;
  }
  default:
    break;
  }
}

void buildDependencies(EqVector &equations, Graph &dependencies) {
  for (EqVector::iterator it = equations.begin(); it != equations.end(); ++it) {
    buildDeps(dependencies, (*it)->var, (*it)->exp);
  }
}

////////////////////////////////////////////////////////////////////////////////
// Topological sort visitor
//
// This visitor merely writes the linear ordering into an
// OutputIterator. The OutputIterator could be something like an
// ostream_iterator, or it could be a back/front_insert_iterator.
// Note that if it is a back_insert_iterator, the recorded order is
// the reverse topological order. On the other hand, if it is a
// front_insert_iterator, the recorded order is the topological
// order.
//
template <typename OutputIterator>
struct mytopo_sort_visitor : public dfs_visitor<>
{
  mytopo_sort_visitor(OutputIterator _iter)
  : m_iter(_iter) { }
    
  template <typename Vertex, typename Graph> 
  void finish_vertex(const Vertex& u, Graph&) { *m_iter++ = u; }
    
  OutputIterator m_iter;
};


// Topological Sort
//
// The topological sort algorithm creates a linear ordering
// of the vertices such that if edge (u,v) appears in the graph,
// then u comes before v in the ordering. The graph must
// be a directed acyclic graph (DAG). The implementation
// consists mainly of a call to depth-first search.
//

template <typename VertexListGraph, typename OutputIterator,
	  typename P, typename T, typename R>
void mytopological_sort(VertexListGraph& g, OutputIterator result,
			const bgl_named_params<P, T, R>& params)
{
  typedef mytopo_sort_visitor<OutputIterator> TopoVisitor;
  depth_first_search(g, params.visitor(TopoVisitor(result)));
}

template <typename VertexListGraph, typename OutputIterator>
void mytopological_sort(VertexListGraph& g, OutputIterator result)
{
  mytopological_sort(g, result, 
		     bgl_named_params<int, buffer_param_t>(0)); // bogus
}
////////////////////////////////////////////////////////////////////////////////

void buildComponents(System &sys, EqMatrix &components) {
#define DUMMY
#ifdef DUMMY
  // TODO fix it and remove this dummy part
  components.resize(1);
  int k = 0;
  foreach_(Equation *eq, sys.equations) {
    components[0][k] = eq;
    k++;
  }
  return;
#endif
  
  EqVector &equations = sys.equations;
  Graph dependencies(sys.size());
  buildDependencies(equations, dependencies);
  vector<int> component(num_vertices(dependencies));
  int num = strong_components(dependencies,
    make_iterator_property_map(component.begin(),
      get(vertex_index, dependencies)));
  vector<int> container;
  mytopological_sort(dependencies, back_inserter(container));
  EqMatrix componentstmp;
  componentstmp.resize(num);
  set<int> cset;
  for (int i = 0; i < component.size(); i++) {
    int c = component[i];
    Equation *eq = equations[i];
    componentstmp[c][eq->var->id] = eq;
    cset.insert(c);
  }
  components.reserve(num);
  for (int i = container.size()-1; i >= 0 ; i--) {
    int c = component[container[i]];
    if (cset.find(c) != cset.end()) {
      EqMap &co = componentstmp[c];
      components.push_back(co);
      cset.erase(c);
    }
  }
}

}
