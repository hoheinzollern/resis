/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "resis.hpp"
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <boost/foreach.hpp>
#define foreach_ BOOST_FOREACH

namespace Resis {

Expression::Expression(ExpType type) : type(type) { }
Expression::~Expression() {}

double ExpConstant::value() {
  return _value;
}

double ExpVariable::value() {
  return _value;
}

double MinExp::value() {
  double result = INFINITY;
  foreach_(Expression *exp, args) {
    double val = exp->value();
    if (val < result) {
      result = val;
    }
  }
  return result;
}

double MaxExp::value() {
  double result = -INFINITY;
  foreach_(Expression *exp, args) {
    double val = exp->value();
    if (val > result) {
      result = val;
    }
  }
  return result;
}

double SumExp::value() {
  double result = 0;
  foreach_(Expression *exp, args) {
    result += exp->value();
  }
  return result;
}

double MulExp::value() {
  assert(args.size() == 2);
  return args[LEFT]->value() * args[RIGHT]->value();
}

double Equation::value() {
  assert(false);
  return 0.;
}

double ExpLP::value() {
  assert(false);
  return 0.;
}

ExpVariable::ExpVariable(uint32_t id, string &name, bool temp)
  : Expression(VARIABLE), temp(temp), id(id), lpid(UNDEFINED), name(name), _value(NAN) { }

bool ExpVariable::isFree() {
  return isnan(_value);
}

bool ExpVariable::isBound() {
  return isfinite(_value) || isinf(_value);
}

void ExpVariable::clear() {
  _value = NAN;
}

ExpConstant::ExpConstant(double value)
  : Expression(CONSTANT), _value(value) { }

Equation::Equation(ExpVariable *var, Expression *exp)
  : Expression(EQUATION), var(var), exp(exp) { }

MinExp::MinExp(Expression *left, Expression *right) :
ExpNary(MIN_EXP, left, right) {};

MaxExp::MaxExp(Expression *left, Expression *right) :
ExpNary(MAX_EXP, left, right) {};

SumExp::SumExp(Expression *left, Expression *right) :
ExpNary(SUM_EXP, left, right) {};

MulExp::MulExp(Expression *left, Expression *right) :
ExpNary(MUL_EXP, left, right) {};

ExpNary::ExpNary(ExpType type, Expression *left, Expression *right) : Expression(type) {
  if ((type == SUM_EXP || type == MIN_EXP) && left->type == type) {
    ExpNary *exp = (ExpNary *)left;
    args.insert(args.end(), exp->args.begin(), exp->args.end());
    delete exp;
  } else {
    args.insert(args.end(), left);
  }
  if ((type == SUM_EXP || type == MIN_EXP) && right->type == type) {
    ExpNary *exp = (ExpNary *)right;
    args.insert(args.end(), exp->args.begin(), exp->args.end());
    delete exp;
  } else {
    args.insert(args.end(), right);
  }
}

ExpNary::ExpNary(ExpType type, ExpVector *args) : Expression(type), args(*args) { }

ExpLP::ExpLP(DblMatrix *A, DblVector *c, ExpVector *args)
  : ExpNary(LP_OP, args), A(*A), c(*c), lpid(UNDEFINED) {
  assert((*A)[0].size() == (*c).size());
  assert((*A).size() == (*args).size());
}

int ExpLP::cols() {
  return c.size();
}

int ExpLP::rows() {
  return A.size();
}

Expression *System::normalizeExpr(Expression *exp, int depth) {
  switch (exp->type) {
  case MAX_EXP:
  case MIN_EXP:
  case LP_OP: {
    if (depth > 0) {
      ExpVariable *fresh_var = createFreshVariable();
      Expression *new_exp = normalizeExpr(exp, 0);
      createEquation(fresh_var, new_exp);
      return fresh_var;
    } else {
      foreach_(Expression *&sub_expr, ((ExpNary *)exp)->args) {
        sub_expr = normalizeExpr(sub_expr, depth + 1);
      }
      return exp;
    }
    break;
  }
  case SUM_EXP:
  case MUL_EXP: {
    foreach_(Expression *&sub_expr, ((ExpNary *)exp)->args) {
      sub_expr = normalizeExpr(sub_expr, depth + 1);
    }
    return exp;
    break;
  }
  case VARIABLE:
  case CONSTANT: {
    return exp;
    break;
  }
  case EQUATION: {
    cerr << "Error: trying to flatten an equation\n";
    exit(1);
    return NULL;
    break;
  }
  }
}

ExpNary *System::createSum(Expression *left, Expression *right) {
  return new SumExp(left, right);
}

ExpNary *System::createMul(Expression *left, Expression *right) {
  assert(left->type == CONSTANT);
  assert(((ExpConstant *)left)->_value >= 0);
  return new MulExp(left, right);
}

ExpNary *System::createMin(Expression *left, Expression *right) {
  return new MinExp(left, right);
}

ExpNary *System::createMax(Expression *left, Expression *right) {
  return new MaxExp(left, right);
}

ExpLP *System::createLP(DblMatrix *A, DblVector *c, ExpVector *args) {
  return new ExpLP(A, c, args);
}

int System::size() {
  return next_free;
}

void System::createEquation(ExpVariable *v, Expression *exp) {
  Equation *eq = new Equation(v, exp);
  int id = v->id;
  equations.resize(next_free);
  equations[id] = eq;
}

ExpVariable *System::createVariable(string &name) {
  VarMap::iterator it = variables.find(name);

  if (it == variables.end()) {
    ExpVariable *result = new ExpVariable(next_free, name, false);
    variables[name] = result;
    next_free++;
    return result;
  } else {
    return it->second;
  }
}

ExpVariable *System::createFreshVariable() {
  stringstream name;
  name << "tmp#" << variables.size();
  string name_s = name.str();
  ExpVariable *result = new ExpVariable(next_free, name_s, true);
  variables[name_s] = result;
  next_free++;
  return result;
}

ExpConstant *System::createConstant(double value) {
  return new ExpConstant(value);
}

void System::normalize() {
  for (int i = 0; i < size(); i++) {
    Equation *eq = equations[i];
    if (eq) {
      eq->exp = normalizeExpr(eq->exp, 0);
    } else {
      ExpVariable *var;
      for (VarMap::iterator it = variables.begin();
      it != variables.end(); ++it) {
        if (it->second->id == i)
          var = it->second;
      }
      cerr << "Equation missing for variable " << *var << endl;
      exit(1);
    }
  }
}

System::System() : next_free(0) { }

void printArgs(ostream &stream, ExpVector &args) {
  int i;
  for (i = 0; i < args.size(); i++) {
    stream << *args[i];
    if (i < args.size() - 1)
      stream << ", ";
  }
}

void printDblArr(ostream &stream, DblVector &args) {
  int i;
  for (i = 0; i < args.size(); i++) {
    stream << args[i];
    if (i < args.size() - 1)
      stream << ", ";
  }
}

void printVector(ostream &stream, DblVector &array) {
  stream << "[";
  printDblArr(stream, array);
  stream << "]";
}

void printMatrix(ostream &stream, DblMatrix &matrix) {
  int i;
  stream << "[";
  for (i = 0; i < matrix.size(); i++) {
    printDblArr(stream, matrix[i]);
    if (i < matrix.size() - 1)
      stream << "; ";
  }
  stream << "]";
}

ostream &operator<<(ostream &stream, Expression &ex) {
  Expression *exp = &ex;
  if (exp->type == VARIABLE) {
    stream << ((ExpVariable *)exp)->name;
  } else if (exp->type == CONSTANT) {
    stream << ((ExpConstant *)exp)->_value;
  } else if (exp->type == LP_OP) {
    stream << "LP<";
    printMatrix(stream, ((ExpLP *)exp)->A);
    stream << ", ";
    printVector(stream, ((ExpLP *)exp)->c);
    stream << ">(";
    printArgs(stream, ((ExpLP *)exp)->args);
    stream << ")";
  } else if (exp->type == EQUATION) {
    Equation *eq = (Equation *)exp;
    stream << *eq->var;
    stream << " = ";
    stream << *eq->exp;
  } else {
    int32_t i;
    string symbol;
    switch (exp->type) {
    case MIN_EXP:
      symbol = " /\\ ";
      break;
    case MAX_EXP:
      symbol = " \\/ ";
      break;
    case SUM_EXP:
      symbol = " + ";
      break;
    case MUL_EXP:
      symbol = " * ";
      break;
    default:
      fprintf(stderr, "This should never happen\n");
      exit(1);
    }
    stream << "(";
    ExpVector &args = ((ExpNary *)exp)->args;
    for (i = 0; i < args.size(); i++) {
      stream << *(args[i]);
      if (i < args.size() - 1)
	stream << symbol;
    }
    stream << ")";
  }
  return stream;
}

ostream &operator<<(ostream &stream, System &sys) {
  stream << "equations" << endl;
  for (int i = 0; i < sys.size(); i++) {
    Equation *eq = sys.equations[i];
    if (eq)
      stream << *eq << endl;
  }
  return stream;
}

}