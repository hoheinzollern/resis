/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "resis.hpp"

#include "components.hpp"

#include "ClpSimplex.hpp"
#include "CoinPackedVector.hpp"

#include <unistd.h>
#include <assert.h>
#include <math.h>
#include <boost/foreach.hpp>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#define foreach_ BOOST_FOREACH

#define SLEEP_TIME 1

using namespace boost::numeric::ublas;

//typedef compressed_matrix<double, row_major, 0, unbounded_array<int>, unbounded_array<double> > CMatrix;
typedef mapped_matrix<double, row_major> CMatrix;
//typedef generalized_vector_of_vector<double, row_major> CMatrix;

namespace Resis {

void computeLPsize(Expression *exp, int &rows, int &cols, int depth) {
  switch(exp->type) {
  case MAX_EXP: {
    cerr << "\\/-expressions are not considered here" << endl;
    exit(1);
    break;
  }
  case MIN_EXP: {
    assert(depth == 0);
    rows += ((ExpNary *)exp)->args.size()-1;
    
    ExpNary *nexp = (ExpNary *)exp;
    foreach_(Expression *e, nexp->args) {
      computeLPsize(e, rows, cols, depth + 1);
    }
    break;
  }
  case SUM_EXP:
  case MUL_EXP: {
    ExpNary *nexp = (ExpNary *)exp;
    foreach_(Expression *e, nexp->args) {
      computeLPsize(e, rows, cols, depth + 1);
    }
    break;
  }
  case LP_OP: {
    assert(depth == 0);
    ExpLP *lpexp = (ExpLP *)exp;
    lpexp->lpid = cols;
    rows += lpexp->rows();
    cols += lpexp->cols();
    
    ExpNary *nexp = (ExpNary *)exp;
    foreach_(Expression *e, nexp->args) {
      computeLPsize(e, rows, cols, depth + 1);
    }
    break;
  }
  case VARIABLE: {
    ExpVariable *vexp = (ExpVariable *)exp;
    if (vexp->isFree() && vexp->lpid == UNDEFINED) {
      vexp->lpid = cols;
      cols += 1;
    }
    break;
  }
  case CONSTANT: {
    break;
  }
  case EQUATION:
    cerr << "Equations are not considered" << endl;
    exit(1);
    break;
  }
}

/**
 * Counts the rows of the LP system corresponding to this set of
 * equations
 */
void computeLPsize(IntVector &strategy, EqMap &component, int &rows, int &cols) {
  rows = 0;
  cols = 0;
  foreach_ (IntEqPair item, component) {
    int i = item.first;
    if (item.second->var->isBound()) {
      continue;
    }
    rows += 1;
    computeLPsize(item.second->var, rows, cols, 1);
    Expression *exp = item.second->exp;
    if (exp->type == MAX_EXP) {
      Expression *s = ((ExpNary *)exp)->args[strategy[i]];
      computeLPsize(s, rows, cols, 0);
    } else {
      computeLPsize(exp, rows, cols, 0);
    }
  }
}

void buildRight(Expression *right_sub, CMatrix &A, double &b, int row_id, bool abstract) {
  switch (right_sub->type) {
  case VARIABLE: {
    ExpVariable *variable = (ExpVariable *)right_sub;
    if (variable->isFree()) {
      A(row_id, variable->lpid) -= 1;
    } else {
      if (abstract) {
        if (variable->_value < INFINITY) {
          b += 0;
        } else {
          b += 1;
        }
      } else {
        b += variable->_value;
      }
    }
    break;
  }
  case CONSTANT: {
    ExpConstant *constant = (ExpConstant *)right_sub;
    if (abstract) {
      if (constant->_value < INFINITY) {
        b += 0;
      } else {
        b += 1;
      }
    } else {
      b += constant->_value;
    }
    break;
  }
  case SUM_EXP: {
    ExpNary *exp = (ExpNary *)right_sub;
    foreach_ (Expression *sub_exp, exp->args) {
      buildRight(sub_exp, A, b, row_id, abstract);
    }
    break;
  }
  case MUL_EXP: {
    ExpNary *exp = (ExpNary *)right_sub;
    assert(exp->args.size()==2);
    ExpConstant *constant = (ExpConstant *)exp->args[LEFT];
    ExpVariable *variable = (ExpVariable *)exp->args[RIGHT];
    assert(constant->type == CONSTANT);
    assert(variable->type == VARIABLE);
    double value = constant->_value;

    if (variable->isFree()) {
      A(row_id, variable->id) -= value;
    } else {
      if (abstract) {
        if (variable->_value < INFINITY) {
          b += 0;
        } else {
          b += value;
        }
      } else {
        b += value * variable->_value;
      }
    }
    break;
  }
  case MIN_EXP:
  case MAX_EXP:
  case LP_OP:
  case EQUATION:
    cerr << "Everything should be in normal form, so this should not happen\n";
    assert(0);
  }
}

typedef pair<int, int> IntPair;
/**
 * Builds the matrix of coefficients and known values
 */
void buildSystem(IntVector &strategy, EqMap &component, CMatrix
                 &A, double *B, int rows, int cols, bool abstract) {
  int row_id = 0;
  for(EqMap::iterator it = component.begin(); it != component.end(); ++it) {
    Expression *exp = it->second->exp;
    ExpVariable *var = it->second->var;

    if (var->isBound()) {
      continue;
    }

    switch (exp->type) {
    case MAX_EXP: {
      A(row_id, var->lpid) += 1;
      // Choose the right subexpression depending on strategy
      int i = strategy[var->id];
      Expression *sub_exp = ((ExpNary *)exp)->args[i];
      buildRight(sub_exp, A, B[row_id], row_id, abstract);
      row_id += 1;
      break;
    }
    case MIN_EXP: {
      foreach_(Expression *sub_exp, ((ExpNary *)exp)->args) {
        A(row_id, var->lpid) += 1;
        buildRight(sub_exp, A, B[row_id], row_id, abstract);
        row_id += 1;
      }
      break;
    }
    case LP_OP: {
      ExpLP *explp = (ExpLP *)exp;
      // Build objective function
      A(row_id, var->lpid) += 1;
      int lp_lpid = explp->lpid;
      for (int i = 0; i < explp->cols(); i++, lp_lpid++) {
        A(row_id, lp_lpid) -= explp->c[i];
      }
      row_id += 1;
      // Build constraints
      for (int i = 0; i < explp->rows(); i++) {
        lp_lpid = explp->lpid;
        for (int j = 0; j < explp->cols(); j++, lp_lpid++) {
          A(row_id, lp_lpid) += explp->A[i][j];
        }
        buildRight(explp->args[i], A, B[row_id], row_id, abstract);
        row_id += 1;
      }
      break;
    }
    case SUM_EXP:
    case MUL_EXP:
    case CONSTANT:
    case VARIABLE: {
      A(row_id, var->lpid) += 1;
      buildRight(exp, A, B[row_id], row_id, abstract);
      row_id += 1;
      break;
    }
    case EQUATION: {
      cerr << "This shouldn't happen" << endl;
      exit(1);
      break;
    }
    }
  }
  assert(rows == row_id);
}

bool improveMaxStrategy(IntVector &strategy, EqMap &component) {
  bool improved = false;
  foreach_(IntEqPair pair, component) {
    int i = pair.first;
    Equation *eq = pair.second;
    if (eq->var->_value != INFINITY && eq->exp->type == MAX_EXP) {
      ExpNary *exp = (ExpNary *)eq->exp;
      ExpVector &args = exp->args;

      int old_strategy = strategy[i];
      double max_value = args[old_strategy]->value();
      for (int j = 0; j < args.size(); j++) {
        if (j != old_strategy) {
          double new_value = args[j]->value();
          if (new_value > max_value) {
            strategy[i] = j;
            max_value = new_value;
            improved = true;
          }
        }
      }
    }
  }
  return improved;
}

void applySolution (double *solution, EqMap &component) {
  foreach_(IntEqPair pair, component) {
    ExpVariable *v = pair.second->var;
    if (v->_value != INFINITY)
      v->_value = solution[v->lpid];
  }
}

void applyInfAbstraction (double *solution, EqMap &component) {
  foreach_(IntEqPair pair, component) {
    ExpVariable *v = pair.second->var;
    if (v->lpid != UNDEFINED && solution[v->lpid] > 0)
      v->_value = INFINITY;
  }
}

void clearValues(EqMap &component) {
  foreach_ (IntEqPair item, component) {
    ExpVariable *var = item.second->var;
    var->lpid = UNDEFINED;
    if (var->_value != INFINITY) {
      var->clear();
    }
  }
}

void compress(CMatrix &A, int *&rowidx, int *&colidx, int *&len, double *&values, int &nnz) {
  int lastRow, i, nRows = A.size1(), nCols = A.size2();
  nnz = A.data().size();
  
  rowidx = new int[nRows + 1];
  colidx = new int[nnz];
  len = new int[nRows + 1];
  values = new double[nnz];

  CMatrix::array_type::iterator it;
  for (it = A.data().begin(), lastRow = -1, i = 0; it != A.data().end(); ++it, ++i) {
    long id = it->first;
    int row = id / nCols;
    int col = id % nCols;
    if (row != lastRow) {
      rowidx[row] = i;
      if (lastRow != -1) {
        len[lastRow] = i - rowidx[lastRow];
      }
      lastRow = row;
    }
    colidx[i] = col;
    values[i] = it->second;
  }
  rowidx[lastRow + 1] = i;
  len[lastRow] = i - rowidx[lastRow];
}

class DerivedHandler :
public CoinMessageHandler {
public:
  virtual int print() ;
};

bool solveLP(EqMap &component, IntVector &strategy, double &objective, bool &feasible, bool &bounded, bool infabs) {
  double *B = NULL;
  double *col_lb = NULL;
  double *col_ub = NULL;
  double *obj = NULL;
  double *values = NULL;
  int *colidx = NULL;
  int *rowidx = NULL;
  int *len = NULL;
  int nRows;
  int nCols;
  int nnz;
  bool improved = false;
  
  ClpSimplex model;
  if (!infabs) {
    DerivedHandler handler;
    model.passInMessageHandler(&handler);
  }
  clearValues(component);
  computeLPsize(strategy, component, nRows, nCols);
  
  CMatrix A(nRows, nCols);
  B = new double[nRows];
  fill_n(B, nRows, 0.);
  col_lb = new double[nCols];
  fill_n(col_lb, nCols, -INFINITY);
  if (infabs) {
    col_ub = new double[nCols];
    fill_n(col_ub, nCols, 1.0);
  }
  obj = new double[nCols];
  foreach_(IntEqPair pair, component) {
    obj[pair.second->var->lpid] = 1.;
  }
  
  buildSystem(strategy, component, A, B, nRows, nCols, infabs);
  compress(A, rowidx, colidx, len, values, nnz);
  A.clear();

  CoinPackedMatrix matrix(false, nCols, nRows, nnz, values, colidx, rowidx, len);
  model.loadProblem(matrix, col_lb, col_ub, obj, NULL, B);
  
  model.setOptimizationDirection(-1);
  model.initialSolve();
  
  feasible = model.primalFeasible();
  bounded = model.dualFeasible();
  
  double *solution = model.primalColumnSolution();
  
  if (infabs) {
    applyInfAbstraction(solution, component);
  } else if (feasible) {
    objective = model.rawObjectiveValue();
    applySolution(solution, component);
    improved = improveMaxStrategy(strategy, component);
  }

// To my knowledge we don't need these, as the library is taking care of such deletes in gutsOfDelete.
//  delete []B;
//  delete []col_lb;
//  if (infabs) delete []col_ub;
//  delete []obj;
  delete []rowidx;
  delete []colidx;
  delete []len;
  delete []values;
  return improved;
}

int DerivedHandler::print()
{ return 0; }

void initializeStrategy(IntVector &strategy, EqMap &component) {
  foreach_ (IntEqPair pair, component) {
    Equation *eq = pair.second;
    if (eq->exp->type == MAX_EXP) {
      strategy[pair.first] = 0;
    } else {
      strategy[pair.first] = UNDEFINED;
    }
  }
}

void solveLPs(System &sys, EqMatrix &components) {
  bool feasible = true, bounded = true;
  double objective = -INFINITY, old_objective = -INFINITY;
  int iterations = 0;
  foreach_ (EqMap &component, components) {
    if (!feasible && !bounded) break;

#ifdef DEBUG
    cerr << "Solving component: ";
    foreach_ (IntEqPair pair, component) {
      cerr << *pair.second->var << " ";
    }
    cerr << endl;
#endif
    
    IntVector strategy(sys.size());
    initializeStrategy(strategy, component);
    improveMaxStrategy(strategy, component);
    bool improved = true;
    while (improved && feasible && (true || old_objective <= objective)) {
      old_objective = objective;
      solveLP(component, strategy, objective, feasible, bounded, true);
      improved = solveLP(component, strategy, objective, feasible, bounded, false);
      iterations += 1;
    }
  }
  if (feasible) {
    for (int i = 0; i < sys.equations.size(); i++) {
      ExpVariable *v = sys.equations[i]->var;
      cout << v->name << " -> " << v->_value << endl;
    }
    if (!bounded) {
      cout << "unbounded" << endl;
    }
    if (old_objective > objective) {
      cout << "warning: the strategy improvement had some errors with monotonicity" << endl;
    }
  } else if (!feasible) {
    cout << "unfeasible" << endl;
  }
}

void System::solve() {
  EqMatrix components;
  buildComponents(*this, components);
  solveLPs(*this, components);
}

}
